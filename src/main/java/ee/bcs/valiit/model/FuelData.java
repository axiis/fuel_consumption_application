package ee.bcs.valiit.model;

public class FuelData {

	private int id;
	private String driverCode;
	private String firstName;
	private String lastName;
	private double fuelPrice;
	private double pricePerLiter;
	private double volumeLiters;
	private String purchasingDate;
	private String fuelType;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getDriverCode() {
		return driverCode;
	}
	public void setDriverCode(String driverCode) {
		this.driverCode = driverCode;
	}
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public double getFuelPrice() {
		return fuelPrice;
	}
	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}
	
	public double getPricePerLiter() {
		return pricePerLiter;
	}
	public void setPricePerLiter(double pricePerLiter) {
		this.pricePerLiter = pricePerLiter;
	}

	public double getVolumeLiters() {
		return volumeLiters;
	}
	public void setVolumeLiters(double volumeLiters) {
		this.volumeLiters = volumeLiters;
	}
	public String getPurchasingDate() {
		return purchasingDate;
	}
	public void setPurchasingDate(String purchasingDate) {
		this.purchasingDate = purchasingDate;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}


	

}
