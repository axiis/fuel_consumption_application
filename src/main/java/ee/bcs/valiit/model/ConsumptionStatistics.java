package ee.bcs.valiit.model;

public class ConsumptionStatistics {
	
	private int year;
	private int month;
	private double totalCost;
	private double averagePrice;
	private double totalVolume;
	private String fuelType;

	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public int getMonth() {
		return month;
	}
	public void setMonth(int month) {
		this.month = month;
	}
	public double getTotalCost() {
		return totalCost;
	}
	public void setTotalCost(double totalCost) {
		this.totalCost = totalCost;
	}
	public double getAveragePrice() {
		return averagePrice;
	}
	public void setAveragePrice(double avaragePrice) {
		this.averagePrice = avaragePrice;
	}
	public double getTotalVolume() {
		return totalVolume;
	}
	public void setTotalVolume(double totalVolume) {
		this.totalVolume = totalVolume;
	}
	public String getFuelType() {
		return fuelType;
	}
	public void setFuelType(String fuelType) {
		this.fuelType = fuelType;
	}

}
