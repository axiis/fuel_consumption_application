package ee.bcs.valiit.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ee.bcs.valiit.model.ConsumptionStatistics;
import ee.bcs.valiit.model.Driver;
import ee.bcs.valiit.model.FuelConsumptionReport;
import ee.bcs.valiit.model.FuelData;
import ee.bcs.valiit.model.FuelType;
import ee.bcs.valiit.services.ConsumptionService;

@Path("/")
public class SwedRestResource {

	@GET
	@Path("/get_fuel_consumption_all_data")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuelData> getFuelConsumptionAllData() {
		return ConsumptionService.getFuelConsumptionAllData();
	}

	@GET
	@Path("/get_one_fuel_data")
	@Produces(MediaType.APPLICATION_JSON)
	public FuelData getOneFuelData(@QueryParam("fuel_data_id") int fuelDataId) {
		return ConsumptionService.getOneFuelData(fuelDataId);
	}

	// LISAME FUEL meie tabelisse
	@POST
	@Path("/add_fuel_data")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addFuelData(FuelData[] fuelData) {
		ConsumptionService.addFuelData(fuelData);
		return "OK";
	}

	@POST
	@Path("/modify_fuel_data")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String modifyFuelData(FuelData fuelData) {
		ConsumptionService.modifyFuelData(fuelData);
		return "OK";
	}

	// lisan funktsiooni DELETE
	@POST
	@Path("/delete_fuel_data")
	@Produces(MediaType.TEXT_PLAIN)
	public String deleteFuelData(@FormParam("fuel_data_id") int fuelDataId) {
		ConsumptionService.deleteFuelData(fuelDataId);
		return "OK";
	}

	// 1. Total spent amount of money grouped by month
	@GET
	@Path("/get_fuel_consumption_cost_for_month")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuelConsumptionReport> getFuelConsumptionReport(@QueryParam("start_year") int startYear,
			@QueryParam("start_month") int startMonth, @QueryParam("month_count") int monthCount, @DefaultValue("") @QueryParam("driver_code") String driverCode) {
		return ConsumptionService.getFuelConsumptionReports(startYear, startMonth, monthCount, driverCode);
	}

	// 2. List fuel consumption records for specified month
	@GET
	@Path("/get_fuel_data_all_by_month")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuelData> getFuelDataAllByMonth(@QueryParam("start_year") int startYear,
			@QueryParam("start_month") int startMonth, @DefaultValue("") @QueryParam("driver_code") String driverCode) {
		return ConsumptionService.getFuelDataAllByMonth(startYear, startMonth, driverCode);
	}

	// 3. Statistic for each month (grouped by Fuel Type).
	@GET
	@Path("/get_consumption_statistics_for_month")
	@Produces(MediaType.APPLICATION_JSON)
	public List<ConsumptionStatistics> getConsumptionStatisticsForMonth(@QueryParam("start_year") int startYear,
			@QueryParam("start_month") int startMonth, @QueryParam("month_count") int monthCount, @DefaultValue("") @QueryParam("driver_code") String driverCode) {
		List<ConsumptionStatistics> statisticsList = new ArrayList<>();
		
		List<String> fuelTypes = ConsumptionService.getFuelTypes();
		for (int i = 0; i < fuelTypes.size(); i++ ) {
			statisticsList.addAll(ConsumptionService.getConsumptionStatisticsByMonths(startYear, startMonth, monthCount, fuelTypes.get(i), driverCode));
			
		}
		return statisticsList;
	}
	
	@GET
	@Path("/get_drivers")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Driver> getDrivers() {
		return ConsumptionService.getDrivers();
	}
	

	// Fuel Type dropdown menu
	@GET
	@Path("/get_fuel_type_menu")
	@Produces(MediaType.APPLICATION_JSON)
	public List<FuelType> getFuelTypeMenu() {
		return ConsumptionService.getFuelTypeMenu();
	}
	
	
	@POST
	@Path("/add_new_driver")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.TEXT_PLAIN)
	public String addDriver(Driver[] drivers) {
		ConsumptionService.addNewDriver(drivers);
		return "OK";
	}
	
	
}
