CREATE DATABASE  IF NOT EXISTS `fuel_consumption` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `fuel_consumption`;
-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: fuel_consumption
-- ------------------------------------------------------
-- Server version	5.7.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `driver`
--

DROP TABLE IF EXISTS `driver`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `driver` (
  `driver_code` varchar(40) NOT NULL,
  `name` varchar(200) DEFAULT NULL,
  `surname` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`driver_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `driver`
--

LOCK TABLES `driver` WRITE;
/*!40000 ALTER TABLE `driver` DISABLE KEYS */;
INSERT INTO `driver` VALUES ('119','Paavo','Huberg'),('120','Marek','Lints'),('121','Katrin','Nevolainen'),('122','Olesja','Smirnov'),('123','Tiit','Kask'),('124','Aivar','Kuusk'),('125','Peep','Kosk'),('126','Oleg','Sokol'),('127','Ilona','Tamberg');
/*!40000 ALTER TABLE `driver` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fuel_data`
--

DROP TABLE IF EXISTS `fuel_data`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fuel_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fuel_price` decimal(10,2) NOT NULL,
  `volume_liters` decimal(10,2) NOT NULL,
  `purchasing_date` date NOT NULL,
  `fuel_type` varchar(10) NOT NULL,
  `driver_code` varchar(40) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fuel_data_fuel_type_idx` (`fuel_type`),
  KEY `fuel_data_driver_idx` (`id`),
  KEY `duel_date_driver_idx` (`driver_code`),
  KEY `fuel_data_driver_idx1` (`driver_code`),
  CONSTRAINT `fuel_data_driver` FOREIGN KEY (`driver_code`) REFERENCES `driver` (`driver_code`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fuel_data_fuel_type` FOREIGN KEY (`fuel_type`) REFERENCES `fuel_type` (`fuel_type`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=167 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fuel_data`
--

LOCK TABLES `fuel_data` WRITE;
/*!40000 ALTER TABLE `fuel_data` DISABLE KEYS */;
INSERT INTO `fuel_data` VALUES (1,59.78,45.77,'2018-05-14','95','121'),(3,30.15,43.58,'2017-05-14','95','122'),(4,48.45,41.57,'2017-09-25','98','124'),(6,15.14,10.00,'2017-12-31','95','124'),(8,55.00,60.00,'2018-05-09','95','121'),(9,48.00,40.00,'2018-02-14','95','125'),(11,50.00,44.49,'2017-11-05','98','124'),(13,51.49,46.12,'2018-04-28','98','119'),(143,1.25,15.78,'2018-05-09','95','120'),(145,50.00,15.78,'2018-05-09','95','122'),(147,45.00,15.78,'2018-05-09','95','119'),(151,45.48,45.00,'2018-05-14','D','119'),(152,65.87,58.92,'2017-04-14','D','123'),(153,34.65,23.98,'2017-03-27','D','123'),(154,65.70,56.30,'2017-02-17','D','123'),(156,100.00,100.00,'2018-05-14','D','123'),(158,100.00,100.00,'2018-05-14','D','123'),(162,345.00,2354.00,'2018-05-29','98','123'),(163,235.00,2354.00,'2018-05-07','D','119'),(164,235.00,2354.00,'2018-05-07','D','119'),(165,44444.00,4444.00,'2018-05-09','98','121'),(166,55555777.00,555.00,'2018-04-29','95','120');
/*!40000 ALTER TABLE `fuel_data` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fuel_type`
--

DROP TABLE IF EXISTS `fuel_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fuel_type` (
  `fuel_type` varchar(10) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`fuel_type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fuel_type`
--

LOCK TABLES `fuel_type` WRITE;
/*!40000 ALTER TABLE `fuel_type` DISABLE KEYS */;
INSERT INTO `fuel_type` VALUES ('95','Fuel 95'),('98','Fuel 98'),('D','Diesel');
/*!40000 ALTER TABLE `fuel_type` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-18 14:03:40
