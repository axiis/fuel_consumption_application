package ee.bcs.valiit.services;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ee.bcs.valiit.model.ConsumptionStatistics;
import ee.bcs.valiit.model.FuelConsumptionReport;
import ee.bcs.valiit.model.FuelData;

public class ConsumptionServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	
	@Test
	public void testGetFuelConsumptionAllData() {
		List<FuelData> fuelDatas = ConsumptionService.getFuelConsumptionAllData();
		assertTrue(fuelDatas.size() > 0);
		assertTrue(fuelDatas.get(0).getFuelType().length() > 0); 
		assertTrue(fuelDatas.get(0).getId() > 0);
	}

	
	@Test
	public void testGetOneFuelData() throws SQLException {
		ResultSet result = ConsumptionService.executeSql("select * from fuel_data");
		assertNotNull(result);
		assertTrue(result.next());
		int id = result.getInt("id");
		assertTrue(id > 0);
		int numberIdTest = result.getInt("id");
		assertTrue(numberIdTest > 0);
	}
	
	@Test
	public void testGetFuelDataFromService() throws SQLException {
		ResultSet result = ConsumptionService.executeSql("select * from fuel_data");
		result.next();
		
		FuelData fuelData = ConsumptionService.getOneFuelData(result.getInt("id"));
		assertTrue(fuelData.getId() > 0); 
	}
	
	
	@Test
	public void testAddFuelData() throws SQLException {    
		FuelData fuelData = new FuelData();
		fuelData.setFuelType("D");
		fuelData.setDriverCode("123");
		fuelData.setFuelPrice(100.0);
		fuelData.setVolumeLiters(100.0);
		fuelData.setPurchasingDate("2018-05-14");
		ConsumptionService.addFuelData(new FuelData[] { fuelData });	
		
		ResultSet result = ConsumptionService.executeSql("select * from fuel_data order by id desc limit 1");
		assertTrue(result.next());
		assertTrue(result.getInt("id") > 0);
		assertTrue(result.getString("fuel_type").equals("D"));
		assertTrue(result.getString("purchasing_date").equals("2018-05-14"));
		
	}
	
//	@Test
//	public void testDeleteFuelData() throws SQLException {    
//		FuelData fuelDataId = new FuelData();
//		fuelDataId.setFuelType("D");
//		fuelDataId.setDriverCode("123");
//		fuelDataId.setFuelPrice(100.0);
//		fuelDataId.setVolumeLiters(100.0);
//		fuelDataId.setPurchasingDate("2018-05-14");
//		ConsumptionService.addFuelData(new FuelData[] { fuelDataId });
//		ResultSet result = ConsumptionService.executeSql("select * from fuel_data where fuel_type='98'");
//		assertTrue(result.next());	
//		
//		ConsumptionService.executeSql("delete from fuel_data where fuel_type='D'");
//		
//		result = ConsumptionService.executeSql("select * from fuel_data where fuel_type='98'");
//		assertFalse(result.next());
//
//	}
	
	// Report 1
	@Test
	public void textGetFuelConsumptionReport() {
		
		List<FuelConsumptionReport> list = ConsumptionService.getFuelConsumptionReports(2017, 1, 15, "123");
		assertTrue(list != null);
		assertTrue(list.size() > 0);
		assertTrue(list.get(0).getTotalCost() == 0); 
	}
	
	
	}
		

	
	
	

